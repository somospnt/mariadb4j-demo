package com.somospnt.demo.repository;

import com.somospnt.demo.domain.Persona;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PersonaRepository extends JpaRepository<Persona, Long> {

    @Query(value = "SELECT * FROM persona WHERE MATCH(nombre, apellido) AGAINST (:query IN BOOLEAN MODE)", nativeQuery = true)
    List<Persona> buscar(@Param("query") String texto);
}
