package com.somospnt.demo.repository;

import com.somospnt.demo.domain.Persona;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PersonaRepositoryTest {
    
    @Autowired
    private PersonaRepository personaRepository;
    
    @Test
    public void buscar_conTextoOk_retornaLista() {
        
        List<Persona> persona = personaRepository.buscar("Coco Toto");
        
        assertTrue(persona.size() > 1);
        
    }
    
}
