CREATE TABLE persona (
    id bigint UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(45) NOT NULL,
    apellido VARCHAR(45) NOT NULL,
    FULLTEXT(nombre,apellido)
);

